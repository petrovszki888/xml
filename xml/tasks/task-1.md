> #### _kódmágia_

# XML készítés

Készítsen a telefonkészülékek adatainak tárolására alkalmas XML állományt.

Egy telefonról a következő adatokat szeretnénk tárolni:

- szín
- gyártó
- asztali (igen, nem)
- gyártás éve


__Segítség__

[Előadás](../materials/definitions1.md)

> #### _kódmágia_
