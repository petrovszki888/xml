> #### _kódmágia_

# DTD készítés

Készítse el azt a DTD állományt, amelynek alapján létrehozható a következő XML fájl.
```
<?xml version="1.0" encoding="UTF-8"?>
<szemely>
  <dolgozo type="állandó">
        <név>Péter</név>
        <az>1000</az>
        <kor>42</kor>
  </dolgozo>
  <dolgozo type="szerzodeses">
        <név>Robert</név>
        <az>1020</az>
        <kor>22</kor>
  </dolgozo>
  <dolgozo type="állandó">
        <név>Lajos</név>
        <az>3123</az>
        <kor>29</kor>
  </dolgozo>
</szemely>
```

__Segítség__

[Előadás](../materials/definitions1.md)

> #### _kódmágia_
