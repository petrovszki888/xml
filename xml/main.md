> #### _kódmágia_

# Móka és kacagás az XML-el

## Tananyag

- [XML alapok](materials/definitions1.md)
- [XML feldolgozás JAVA-ban](materials/definitions2.md)

## Feladatok

- [XML készítés](tasks/task-1.md) 
- [XSD készítés](tasks/task-2.md) 
- [DTD készítés](tasks/task-3.md) 
- [DOM feldolgozás](tasks/task-4.md) 
- [SAX feldolgozás](tasks/task-5.md) 

> #### _kódmágia_
