> #### _kódmágia_

# Feladat neve, pl.: Tanulási statisztika gyűjtő (ne az hogy az ArrayList vs LikedList)

Képzeljük el, hogy van egy alkalmazásunk ami gyűjti, hogy mikor tanultunk programozni. A programot egyszerű parancsokkal szeretnénk irányítani, mint:

- ```learn```: Jelezzük a programnak, hogy tanultunk. A program ezután meg kell, hogy kérdezze, hogy milyen módon és mennyit.
- ```learn-list```: Írja ki az összes alkalmat, amikor tanultunk.
- ```learn-stat```: Írjon ki némi statisztikát arról, hogy hogyan tanultunk. Összegezze a különböző kategóriákba tartozó alkalmakat.

__Példa__

![Működés](gifs/learning-tracker.gif)

__Megoldás__

Leírás, hogy hogyan oldottad meg, bármilyen gondolat jöhet még ami hasznos, motiváló, bemutatja, hogy hol fordulhat még elő ilyesmi...

``` LearningTracker.java ```

```java
package codeismagic.stringprocessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LearningTracker {

	public static void main(String[] args) {
		List<String> learningActivities = new ArrayList<>();

		Scanner scanner = new Scanner(System.in);
		String command;
		while (!(command = scanner.nextLine().trim()).equals("exit")) {
			if ("learn".equals(command)) {
				System.out.println("Learning type:");
				String learningType = scanner.nextLine().trim();
				System.out.println("Duration in minutes:");
				int duration = scanner.nextInt();
				learningActivities.add(learningType + "," + duration);
				System.out.println("Good! I saved your activity :)!");
			} else if ("learn-list".equals(command)) {
				for (String learningActivity : learningActivities) {
					System.out.println(learningActivity);
				}
			} else if ("learn-stat".equals(command)) {
				Map<String, Integer> activitiesWithTime = new HashMap<>();
				for (String learningActivity : learningActivities) {
					String[] values = learningActivity.split(",");
					if (activitiesWithTime.containsKey(values[0])) {
						int duration = Integer.parseInt(values[1]);
						int previousSum = activitiesWithTime.get(values[0]);
						activitiesWithTime.put(values[0], previousSum + duration);
					} else {
						int duration = Integer.parseInt(values[1]);
						activitiesWithTime.put(values[0], duration);
					}
				}
				System.out.println(activitiesWithTime);
			}
		}
	}
}
```

> #### _kódmágia_
